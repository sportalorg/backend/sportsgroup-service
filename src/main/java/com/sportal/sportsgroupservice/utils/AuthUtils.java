package com.sportal.sportsgroupservice.utils;

import com.google.gson.Gson;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import java.util.Base64;
import java.util.Map;

@Component
@RequiredArgsConstructor
@Slf4j
public class AuthUtils {
    public String getField(String accessToken,String field) {
        String[] chunks = accessToken.split("\\.");
        Base64.Decoder decoder = Base64.getUrlDecoder();
        String payload = new String(decoder.decode(chunks[1]));
        Map jsonToken = new Gson().fromJson(payload, Map.class);
        return jsonToken.get(field).toString();
    }
}
