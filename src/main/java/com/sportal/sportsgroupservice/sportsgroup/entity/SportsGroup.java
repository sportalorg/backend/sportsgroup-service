package com.sportal.sportsgroupservice.sportsgroup.entity;

import com.sportal.sportsgroupservice.sportsgroup.entity.gametemplate.GameTemplate;
import com.sportal.sportsgroupservice.sportsgroup.entity.subscription.Subscription;
import lombok.Builder;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Data
@Builder
@Document(collection = "sportsgroups")
public class SportsGroup {

    @Id
    private final String id;
    private String name;
    private String homeLocation;
    private List<Subscription> subscriptions;
    private GameTemplate gameTemplate;

}
