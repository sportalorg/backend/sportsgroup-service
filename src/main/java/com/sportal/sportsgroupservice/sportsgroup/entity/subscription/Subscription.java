package com.sportal.sportsgroupservice.sportsgroup.entity.subscription;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Subscription {

    private String athleteId;
    private SubscriptionStatus status;
    private Boolean admin;


}
