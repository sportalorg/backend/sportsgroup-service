package com.sportal.sportsgroupservice.sportsgroup.entity.subscription;

public enum SubscriptionStatus {
    PENDING,
    APPROVED,
    REJECTED
}
