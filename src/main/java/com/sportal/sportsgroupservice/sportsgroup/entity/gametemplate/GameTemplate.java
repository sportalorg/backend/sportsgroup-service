package com.sportal.sportsgroupservice.sportsgroup.entity.gametemplate;

import lombok.Builder;
import lombok.Data;

import java.time.DayOfWeek;
import java.time.LocalTime;

@Data
@Builder
public class GameTemplate {

    private Long teamSize;
    private Sports sport;
    private DayOfWeek day;
    private LocalTime time;
}
