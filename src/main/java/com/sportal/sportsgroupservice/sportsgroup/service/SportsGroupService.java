package com.sportal.sportsgroupservice.sportsgroup.service;

import com.sportal.sportsgroupservice.sportsgroup.dto.SportsGroupGameTemplateResponse;
import com.sportal.sportsgroupservice.sportsgroup.dto.SportsGroupRegistrationRequest;
import com.sportal.sportsgroupservice.sportsgroup.dto.SportsGroupResponse;
import com.sportal.sportsgroupservice.sportsgroup.dto.SubscriptionResponse;
import com.sportal.sportsgroupservice.sportsgroup.entity.SportsGroup;
import com.sportal.sportsgroupservice.sportsgroup.entity.gametemplate.GameTemplate;
import com.sportal.sportsgroupservice.sportsgroup.entity.gametemplate.Sports;
import com.sportal.sportsgroupservice.sportsgroup.entity.subscription.Subscription;
import com.sportal.sportsgroupservice.sportsgroup.entity.subscription.SubscriptionStatus;
import com.sportal.sportsgroupservice.sportsgroup.repository.SportsGroupRepository;
import com.sportal.sportsgroupservice.utils.AuthUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Slf4j
public class SportsGroupService {

    private final SportsGroupRepository sportsGroupRepository;
    private final AuthUtils authUtils;

    public SportsGroup getSportsGroupDetails(String id){
        Optional<SportsGroup> optGroup = sportsGroupRepository.findById(id);
        return optGroup.orElse(null);
    }

    public SportsGroup registerSportsGroup(String adminId, SportsGroupRegistrationRequest request) {

        List<Subscription> subList = new ArrayList<>();

        Subscription subscription = Subscription.builder()
                .athleteId(adminId)
                .admin(true)
                .status(SubscriptionStatus.APPROVED)
                .build();

        subList.add(subscription);

        SportsGroup sportsGroup = SportsGroup.builder()
                .name(request.name())
                .homeLocation(request.homeLocation())
                .subscriptions(subList)
                .gameTemplate(GameTemplate.builder()
                        .teamSize(request.teamSize())
                        .day(request.defaultDay())
                        .sport(Sports.FOOTBALL)
                        .time(request.defaultTime()).build())
                .build();

        return sportsGroupRepository.save(sportsGroup);
    }

    public SportsGroup setAdmin(String adminId, String newAdmin, String groupId) {

        Optional<SportsGroup> optGroup = sportsGroupRepository.findById(groupId);
        if(optGroup.isPresent()){
            List<Subscription> savedSubs = optGroup.get().getSubscriptions();
            if(!savedSubs.stream().filter(s -> s.getAthleteId().toString().equals(adminId.toString()) &&
                    s.getAdmin().equals(true)).toList().isEmpty()){

                for(Subscription sub : savedSubs){
                    if(sub.getAthleteId().toString().equals(newAdmin.toString())){
                        sub.setAdmin(true);
                        sub.setStatus(SubscriptionStatus.APPROVED);
                    }
                }
                SportsGroup group = optGroup.get();
                group.setSubscriptions(savedSubs);
                sportsGroupRepository.save(group);
                return group;
            }
        }
        return null;
    }

    public SubscriptionResponse toggleSubscription(String athleteId, String groupId){

        Optional<SportsGroup> optGroup = sportsGroupRepository.findById(groupId);

        if(optGroup.isPresent()){

            SportsGroup group = optGroup.get();

            Subscription newSub = Subscription.builder()
                    .athleteId(athleteId)
                    .status(SubscriptionStatus.APPROVED) //default is approved
                    .admin(false)
                    .build();

            List<Subscription> savedSubs = group.getSubscriptions();
            if(savedSubs.stream().filter(s -> s.getAthleteId().equals(athleteId)).toList().isEmpty()) {
                savedSubs.add(newSub);
                group.setSubscriptions(savedSubs);
                sportsGroupRepository.save(group);

                return SubscriptionResponse.builder()
                        .sportsGroupId(group.getId())
                        .athleteId(newSub.getAthleteId())
                        .admin(newSub.getAdmin())
                        .status(newSub.getStatus())
                        .build();
            } else {
                List<Subscription> entriesToRemove = savedSubs.stream().filter(entry -> entry.getAthleteId().equals(newSub.getAthleteId())).collect(Collectors.toList());
                savedSubs.removeAll(entriesToRemove);
                group.setSubscriptions(savedSubs);
                sportsGroupRepository.save(group);

                return SubscriptionResponse.builder()
                        .sportsGroupId(group.getId())
                        .athleteId(newSub.getAthleteId())
                        .admin(newSub.getAdmin())
                        .status(newSub.getStatus())
                        .build();
            }
        }
        return null;
    }

    public Page<SportsGroup> getSportsGroupsPaginated(Pageable p) {
        return sportsGroupRepository.findAll(p);
    }

    public List<SportsGroup> getMySubscriptions(String athleteId){
        return sportsGroupRepository.findGroupBySubscriber(athleteId);
    }

    public List<String> getSportsGroupsIdsFromMySubscriptions(String athleteId){
        return sportsGroupRepository.findGroupBySubscriber(athleteId).stream()
                .map(SportsGroup::getId).collect(Collectors.toList());
    }

    public HttpStatus isAdmin(String groupId, String accessToken) {

        String sub = authUtils.getField(accessToken, "sub");

        Optional<SportsGroup> optGroup = sportsGroupRepository.findById(groupId);

        if(optGroup.isPresent()){

            Optional<Subscription> optSub = optGroup.get().getSubscriptions()
                    .stream()
                    .filter(p -> p.getAthleteId().equals(sub))
                    .findFirst();

            if(optSub.isPresent()){
                if(optSub.get().getAdmin()){
                    return HttpStatus.OK;
                }
            }
        } else {
            return HttpStatus.NOT_FOUND;
        }
        return HttpStatus.UNAUTHORIZED;
    }

    public SportsGroupGameTemplateResponse getGameTemplate(String groupId){
        Optional<SportsGroup> optGroup = sportsGroupRepository.findById(groupId);

        return optGroup.map(sportsGroup -> SportsGroupGameTemplateResponse.builder()
                .groupId(sportsGroup.getId())
                .teamSize(sportsGroup.getGameTemplate().getTeamSize())
                .sport(sportsGroup.getGameTemplate().getSport())
                .day(sportsGroup.getGameTemplate().getDay())
                .time(sportsGroup.getGameTemplate().getTime())
                .build()).orElse(null);

    }
}
