package com.sportal.sportsgroupservice.sportsgroup.repository;

import com.sportal.sportsgroupservice.sportsgroup.entity.SportsGroup;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SportsGroupRepository extends MongoRepository<SportsGroup, String> {

    @Query("{'subscriptions.athleteId': ?0}")
    List<SportsGroup> findGroupBySubscriber(String athleteId);

}
