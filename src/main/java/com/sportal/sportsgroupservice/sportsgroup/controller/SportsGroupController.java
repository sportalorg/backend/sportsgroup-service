package com.sportal.sportsgroupservice.sportsgroup.controller;

import com.sportal.sportsgroupservice.sportsgroup.dto.SportsGroupGameTemplateResponse;
import com.sportal.sportsgroupservice.sportsgroup.dto.SportsGroupRegistrationRequest;
import com.sportal.sportsgroupservice.sportsgroup.dto.SubscriptionResponse;
import com.sportal.sportsgroupservice.sportsgroup.entity.SportsGroup;
import com.sportal.sportsgroupservice.sportsgroup.service.SportsGroupService;
import com.sportal.sportsgroupservice.utils.AuthUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequiredArgsConstructor
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class SportsGroupController {

    private final AuthUtils authUtils;
    private final SportsGroupService sportsGroupService;

    @GetMapping("/sportsgroup/{id}")
    public ResponseEntity<?> getSportsGroupDetails(@PathVariable String id) {
        final var sportsGroup = sportsGroupService.getSportsGroupDetails(id);
        if (sportsGroup != null) {
            return new ResponseEntity<>(sportsGroup, HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);

    }

    @PostMapping("/sportsgroup")
    public ResponseEntity<?> registerSportsGroup(
            @RequestHeader(name = "Authorization") String accessToken,
            @RequestBody SportsGroupRegistrationRequest sportsGroupRegistrationRequest) {
        SportsGroup response = sportsGroupService.registerSportsGroup(authUtils.getField(accessToken, "sub"), sportsGroupRegistrationRequest);
        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }

    @PutMapping("/sportsgroup/{id}/addadmin/{newAdminId}")
    public ResponseEntity<?> addAdmin(
            @RequestHeader(name = "Authorization") String accessToken,
            @PathVariable String id,
            @PathVariable String newAdminId) {
        SportsGroup response = sportsGroupService.setAdmin(authUtils.getField(accessToken, "sub"),newAdminId, id);
        if (response == null){
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        }
        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }

    @PutMapping("/sportsgroup/{id}/togglesubscribe")
    public ResponseEntity<?> subscribeToGroup(
            @RequestHeader(name = "Authorization") String accessToken,
            @PathVariable String id) {
        SubscriptionResponse response = sportsGroupService.toggleSubscription(authUtils.getField(accessToken, "sub"), id);
        if (response == null){
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        }
        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }

    @GetMapping("/sportsgroup")
    public ResponseEntity<?> getSportsGroupsByPage(Pageable p) {
        Page<SportsGroup> response = sportsGroupService.getSportsGroupsPaginated(p);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping("/sportsgroup/mysubscriptions")
    public ResponseEntity<?> getSportsGroupsByPage(
            @RequestHeader(name = "Authorization") String accessToken) {
        List<SportsGroup> response = sportsGroupService.getMySubscriptions(authUtils.getField(accessToken, "sub"));
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping("/sportsgroup/mysubscriptions/groupids")
    public ResponseEntity<?> getSportsGroupIdsFromSubscriptions(
            @RequestHeader(name = "Authorization") String accessToken) {
        List<String> response =
                sportsGroupService.getSportsGroupsIdsFromMySubscriptions(authUtils.getField(accessToken, "sub"));
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping("/sportsgroup/{id}/admincheck")
    public ResponseEntity<?> isAdminCheck(
            @RequestHeader(name = "Authorization") String accessToken,
            @PathVariable String id) {

        return new ResponseEntity<>(sportsGroupService.isAdmin(id, accessToken));

    }

    @GetMapping("/sportsgroup/{id}/gametemplate")
    public ResponseEntity<?> getGameTemplate(@PathVariable String id) {

        SportsGroupGameTemplateResponse response =
            sportsGroupService.getGameTemplate(id);
        if(response == null){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(response, HttpStatus.OK);

    }


}
