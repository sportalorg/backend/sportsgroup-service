package com.sportal.sportsgroupservice.sportsgroup.dto;

import com.sportal.sportsgroupservice.sportsgroup.entity.gametemplate.Sports;
import lombok.Builder;

import java.time.DayOfWeek;
import java.time.LocalTime;

@Builder
public record SportsGroupGameTemplateResponse (
        String groupId,
        Long teamSize,
        Sports sport,
        DayOfWeek day,
        LocalTime time
) { }
