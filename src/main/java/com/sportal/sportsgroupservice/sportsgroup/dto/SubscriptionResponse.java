package com.sportal.sportsgroupservice.sportsgroup.dto;

import com.sportal.sportsgroupservice.sportsgroup.entity.subscription.SubscriptionStatus;
import lombok.Builder;

@Builder
public record SubscriptionResponse(
        String sportsGroupId,
        String athleteId,
        SubscriptionStatus status,
        Boolean admin
) {
}
