package com.sportal.sportsgroupservice.sportsgroup.dto;

import java.time.DayOfWeek;
import java.time.LocalTime;

public record SportsGroupRegistrationRequest(
        String name,
        String homeLocation,
        LocalTime defaultTime,
        DayOfWeek defaultDay,

        Long teamSize
) { }
