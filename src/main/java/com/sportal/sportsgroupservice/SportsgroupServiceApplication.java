package com.sportal.sportsgroupservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SportsgroupServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(SportsgroupServiceApplication.class, args);
	}

}
